package com.example.mmar12.sandbox;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DecimalFormat;

public final class MoneyUtils implements Serializable {
    private static final long serialVersionUID = -1346766289556158251L;

    private static DecimalFormat df = new DecimalFormat("#,##0.00");

    public static String format(BigDecimal value) {
        return value != null ? df.format(value) : null;
    }

    public static String format(Integer value) {
        return value != null ? value.toString() : null;
    }

    public static String format(Float value) {
        return value != null ? df.format(value) : null;
    }

    public static String formatWithPln(BigDecimal value) {
        return format(value) != null ? format(value).isEmpty() ? null : format(value) + " PLN" : null;
    }

    public static String formatWithWaluta(BigDecimal value, String waluta) {
        return format(value) != null ? format(value).isEmpty() ? null : format(value) + " " + waluta : null;
    }
}
