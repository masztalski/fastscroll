package com.example.mmar12.sandbox;

import android.support.annotation.Nullable;

import java.util.UUID;

public class Comment {
    private UUID mLocalID;
    private String mForeignID;
    private Long mDate;
    private String mContactName;
    private String mPerson;
    private String mCommentKind;
    private String mCommentTXT;
    private String mAnnotaions;
    private boolean mIsSelected; //only for Adapter

    public Comment() {
    }

    public Comment(String foreignID, long date, String contactName, String person, String commentKind, @Nullable String commentTxt, String annotations) {
        mLocalID = UUID.randomUUID();
        mForeignID = foreignID;
        mDate = date;
        mContactName = contactName;
        mPerson = person;
        mCommentKind = commentKind;
        mCommentTXT = commentTxt;
        mAnnotaions = annotations;
    }

    public boolean isSelected() {
        return mIsSelected;
    }

    public void setSelected(boolean selected) {
        mIsSelected = selected;
    }

    public UUID getLocalID() {
        return mLocalID;
    }

    public void setLocalID(UUID localID) {
        mLocalID = localID;
    }

    public String getForeignID() {
        return mForeignID;
    }

    public void setForeignID(String foreignID) {
        mForeignID = foreignID;
    }

    public Long getDate() {
        return mDate;
    }

    public void setDate(Long date) {
        mDate = date;
    }

    public String getContactName() {
        return mContactName;
    }

    public void setContactName(String contactName) {
        mContactName = contactName;
    }

    public String getPerson() {
        return mPerson;
    }

    public void setPerson(String person) {
        mPerson = person;
    }

    public String getCommentKind() {
        return mCommentKind;
    }

    public void setCommentKind(String commentKind) {
        mCommentKind = commentKind;
    }

    public String getCommentTXT() {
        return mCommentTXT;
    }

    public void setCommentTXT(String commentTXT) {
        mCommentTXT = commentTXT;
    }

    public String getAnnotaions() {
        return mAnnotaions;
    }

    public void setAnnotaions(String annotaions) {
        mAnnotaions = annotaions;
    }
}

