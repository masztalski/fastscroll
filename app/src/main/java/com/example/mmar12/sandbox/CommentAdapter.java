package com.example.mmar12.sandbox;

import com.annimon.stream.Optional;
import com.annimon.stream.Stream;
import com.example.mmar12.sandbox.databinding.CommentListItemBinding;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.KomentarzHolder> {
    private List<Comment> mCommentList = Collections.emptyList();
    private CommentCallback mClickCallback;
    private UUID mLastClickedId;

    @Override
    public KomentarzHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new KomentarzHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.comment_list_item, parent, false));
    }

    @Override
    public int getItemCount() {
        return mCommentList.size();
    }

    @Override
    public void onBindViewHolder(KomentarzHolder holder, int position) {
        Comment comment = mCommentList.get(position);
        holder.mSelectedComment = comment;
        holder.mBinding.setComment(comment);
    }

    public void setLastClickedId(UUID lastClickedId) {
        mLastClickedId = lastClickedId;
    }

    class KomentarzHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final CommentListItemBinding mBinding;
        Comment mSelectedComment;

        KomentarzHolder(View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            notifyItemChanged(getAdapterPosition());
            if (mClickCallback != null) mClickCallback.onItemClick(mSelectedComment);
            if (mLastClickedId != mCommentList.get(getAdapterPosition()).getLocalID()) {
                Optional<Comment> optional = Stream.of(mCommentList).filter(k -> k.getLocalID().equals(mLastClickedId)).findFirst();
                if (optional.isPresent()) {
                    optional.get().setSelected(false);
                    notifyItemChanged(mCommentList.indexOf(optional.get()));
                }
            }
            mLastClickedId = mSelectedComment.getLocalID();
        }
    }

    public interface CommentCallback {
        void onItemClick(Comment comment);
    }

    public void setCommentsList(List<Comment> commentList) {
        mCommentList = commentList != null ? commentList : Collections.emptyList();
        notifyDataSetChanged();
    }

    public void setClickCallback(CommentCallback clickCallback) {
        mClickCallback = clickCallback;
    }
}

