package com.example.mmar12.sandbox;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public final class DateTimeFormatUtil {
    private static DateTimeFormatter mSimpleDate = DateTimeFormat.forPattern("dd/MM/yyyy");
    private static DateTimeFormatter mTimeStamp = DateTimeFormat.forPattern("yyyy-MM-dd_k-mm-ss");
    private static DateTimeFormatter mInterwencjaTime = DateTimeFormat.forPattern("dd/MM/yyyy k:mm");

    public static String format(Long date) {
        return date != null ? mSimpleDate.print(date) : null;
    }

    public static String format(DateTime date) {
        return date != null ? mSimpleDate.print(date) : null;
    }

    public static DateTime newDate(DateTime date, int year, int monthOfYear, int dayOfMonth) {
        return date.withDate(year, monthOfYear + 1, dayOfMonth).withMillisOfDay(0);
    }

    public static DateTime newDate(int year, int monthOfYear, int dayOfMonth) {
        return newDate(new DateTime(), year, monthOfYear, dayOfMonth);
    }

    public static DateTime parseStringDate(String stringDate) {
        return mSimpleDate.parseDateTime(stringDate).withMillisOfDay(0);
    }

    public static String formatTimeStamp(DateTime dateTime) {
        return dateTime != null ? mTimeStamp.print(dateTime) : null;
    }

    public static String formatCzasInterwencji(DateTime dateTime){
        return dateTime != null ? mInterwencjaTime.print(dateTime) : null;
    }
}

