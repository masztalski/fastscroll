package com.example.mmar12.sandbox;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;

import com.example.mmar12.sandbox.databinding.CommentListBinding;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends BaseActivity implements CommentAdapter.CommentCallback {
    private CommentAdapter mCommentAdapter = new CommentAdapter();
    private CommentListBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.comment_list);

        LinearLayoutManager lm = new LinearLayoutManager(this);
        lm.setAutoMeasureEnabled(false);
        mBinding.listView.setLayoutManager(lm);
        mBinding.listView.setAdapter(mCommentAdapter);
        mCommentAdapter.setClickCallback(this);

        List<Comment> commentList = new ArrayList<>();
        commentList.add(new Comment("1",System.currentTimeMillis(),"whatever","John Doe","Kind one","Lorem ipsum", "zero"));
        commentList.add(new Comment("1",System.currentTimeMillis(),"whatever2","John X","Kind one","Lorem ipsum", "zero"));
        commentList.add(new Comment("1",System.currentTimeMillis(),"whatever3","John A","Kind one","Lorem ipsum", "zero"));
        commentList.add(new Comment("1",System.currentTimeMillis(),"whatever56","John CYX","Kind one","Lorem ipsum", "zero"));
        commentList.add(new Comment("1",System.currentTimeMillis(),"whatever6789","John POK","Kind one","Lorem ipsum", "zero"));
        commentList.add(new Comment("1",System.currentTimeMillis(),"whatever32","John MOB","Kind one","Lorem ipsum", "zero"));
        commentList.add(new Comment("1",System.currentTimeMillis(),"whatever74","John ASS","Kind one","Lorem ipsum", "zero"));
        mCommentAdapter.setCommentsList(commentList);

        onItemClick(commentList.get(0));
        mCommentAdapter.setLastClickedId(commentList.get(0).getLocalID());
    }

    @Override
    public void onItemClick(Comment comment) {
        comment.setSelected(true);
        mBinding.setComment(comment);
    }
}
