package com.example.mmar12.sandbox;

import android.content.Context;
import android.content.res.TypedArray;
import android.databinding.BindingAdapter;
import android.databinding.DataBindingUtil;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.example.mmar12.sandbox.databinding.VerticalLabeledValueBinding;

public class VerticalLabeledValue extends LinearLayout {

    private VerticalLabeledValueBinding mBinding;

    public VerticalLabeledValue(Context context) {
        super(context);
        init(context, null);
    }

    public VerticalLabeledValue(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public VerticalLabeledValue(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    public VerticalLabeledValue(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        mBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.vertical_labeled_value, this, true);
        setOrientation(LinearLayout.VERTICAL);
        if (attrs == null)
            return;
        TypedArray a = getContext().getTheme().obtainStyledAttributes(attrs, R.styleable.VerticalLabeledValue, 0, 0);
        try {
            int lines = a.getInt(R.styleable.VerticalLabeledValue_lines, -1);
            if (lines != -1) {
                if (lines == 2) {
                    mBinding.verticalLabeledSecondLine.setVisibility(View.VISIBLE);
                    mBinding.verticalLabeledSecondLine.setEllipsize(TextUtils.TruncateAt.END);
                    mBinding.verticalLabeledSecondLine.setLines(1);
                    mBinding.verticalLabeledValue.setLines(1);
                    mBinding.verticalLabeledValue.setEllipsize(TextUtils.TruncateAt.END);
                }
                if (lines == 1) {
                    mBinding.verticalLabeledValue.setLines(lines);
                    mBinding.verticalLabeledValue.setEllipsize(TextUtils.TruncateAt.END);
                }
            }
        } finally {
            a.recycle();
        }
    }

    @BindingAdapter(value = {"bind:verticalLabeledTitle", "bind:verticalLabeledValue", "bind:secondLineValue"}, requireAll = false)
    public static void bindValues(VerticalLabeledValue view, String title, String value, String secondLine) {
        if (title == null || value == null || value.isEmpty()) {
            view.setVisibility(GONE);
        } else {
            view.setVisibility(VISIBLE);
            view.mBinding.verticalLabeledTitle.setText(title);
            view.mBinding.verticalLabeledValue.setText(value);
            if (secondLine != null) view.mBinding.verticalLabeledSecondLine.setText(secondLine);
        }
    }
}

